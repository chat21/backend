import { Message } from '../entities/message.entity';

export class MessageResponse {
  id: string;

  userId: string;

  chatId: string;

  authorName: string;

  text: string;

  static create(message: Message): MessageResponse {
    const authorName = message?.user?.username;
    return {
      id: message.id,
      userId: message.userId,
      chatId: message.chatId,
      authorName: authorName,
      text: message.text,
    };
  }

  static createMany(messages: Message[]): MessageResponse[] {
    return messages.map((m) => this.create(m));
  }
}
