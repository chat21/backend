import { Chat } from '../entities/chat.entity';

export class ChatResponse {
  id: string;

  title: string;

  static create(chat: Chat): ChatResponse {
    let title: string;
    if (chat.participants.length === 1) {
      title = 'Finding...';
    } else {
      title = 'Chat with: ';
      const usernames = [];
      chat.participants.forEach((p) => {
        usernames.push(p.user.username);
      });
      title += usernames.join(', ');
    }

    return {
      id: chat.id,
      title: title,
    };
  }

  static createMany(chats: Chat[]): ChatResponse[] {
    return chats.map((chat) => this.create(chat));
  }
}
