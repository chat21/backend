import { ChatsRepository } from '../repositories/chats.repository';
import { Injectable } from '@nestjs/common';
import { Chat } from '../entities/chat.entity';
import { ParticipantsRepository } from '../repositories/participants.repository';

@Injectable()
export class ChatsService {
  constructor(
    private chatsRepo: ChatsRepository,
    private participantsRepo: ParticipantsRepository,
  ) {}

  async createChat(): Promise<Chat> {
    const chat = this.chatsRepo.create();
    return this.chatsRepo.save(chat);
  }

  async joinToChat(userId: string, chatId: string): Promise<Chat> {
    await this.chatsRepo.findOneOrFail(chatId);
    await this.participantsRepo.save({ userId, chatId });
    return this.findOne(chatId);
  }

  async findOne(id: string): Promise<Chat> {
    return this.chatsRepo.findOneOrFail(id, {
      relations: ['participants', 'messages', 'participants.user'],
    });
  }

  async findAll(userId: string): Promise<Chat[]> {
    const chats = await this.chatsRepo.find({
      relations: ['participants', 'participants.user'],
    });

    return chats.filter((c) => c.participants.some((p) => p.userId === userId));
  }
}
