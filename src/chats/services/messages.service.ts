import { Injectable } from '@nestjs/common';
import { MessagesRepository } from '../repositories/message.repository';
import { Message } from '../entities/message.entity';
import { CreateMessageDto } from '../dto/create-message.dto';

@Injectable()
export class MessagesService {
  constructor(private messagesRepo: MessagesRepository) {}

  async create(userId: string, dto: CreateMessageDto): Promise<Message> {
    const msg = await this.messagesRepo.save({ ...dto, userId });
    return this.messagesRepo.findOneOrFail(msg.id, {
      relations: ['user'],
    });
  }

  async findAll(chatId: string): Promise<Message[]> {
    return this.messagesRepo.find({
      relations: ['user'],
      where: { chatId },
      order: {
        createdAt: 'ASC',
      },
    });
  }
}
