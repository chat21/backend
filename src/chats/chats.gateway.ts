import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { WsJwtAuthGuard } from 'src/auth/ws-jwt-auth.guard';
import { ChatsService } from './services/chats.service';
import { MessagesService } from './services/messages.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { ChatResponse } from './dto/chat.response';
import { MessageResponse } from './dto/message.response';
import { User } from 'src/users/user.entity';
import { Chat } from './entities/chat.entity';

@WebSocketGateway({ cors: true })
@UseGuards(WsJwtAuthGuard)
export class ChatsGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  private server: Server;
  private emptyChatId: string;

  constructor(
    private chatsService: ChatsService,
    private messagesService: MessagesService,
  ) {}

  @SubscribeMessage('createChat')
  async handleCreateChat(@ConnectedSocket() client: Socket): Promise<void> {
    const user: User = client.handshake['user'];
    let chat: Chat;
    if (this.emptyChatId) {
      chat = await this.chatsService.joinToChat(user.id, this.emptyChatId);
      this.emptyChatId = null;
    } else {
      chat = await this.chatsService.createChat();
      chat = await this.chatsService.joinToChat(user.id, chat.id);
      this.emptyChatId = chat.id;
    }

    client.join(chat.id);

    this.server.to(chat.id).emit('chatToClient', ChatResponse.create(chat));
  }

  @SubscribeMessage('joinChat')
  async handleJoinChat(
    @ConnectedSocket() client: Socket,
    @MessageBody('chatId', ParseUUIDPipe) chatId: string,
  ): Promise<void> {
    client.rooms.clear();
    client.join(chatId);

    const messages = await this.messagesService.findAll(chatId);

    this.server
      .to(client.id)
      .emit('messagesToChat', MessageResponse.createMany(messages));
  }

  @UseGuards(WsJwtAuthGuard)
  @SubscribeMessage('getChats')
  async handleGetChats(@ConnectedSocket() client: Socket): Promise<void> {
    const user = client.handshake['user'];

    const chats = await this.chatsService.findAll(user.id);

    this.server
      .to(client.id)
      .emit('chatsToClient', ChatResponse.createMany(chats));
  }

  @SubscribeMessage('sendMessage')
  async handleSendMessage(
    @ConnectedSocket() client: Socket,
    @MessageBody() dto: CreateMessageDto,
  ): Promise<void> {
    const user = client.handshake['user'];

    const message = await this.messagesService.create(user.id, dto);

    this.server
      .to(message.chatId)
      .emit('messageToChat', MessageResponse.create(message));
  }

  handleConnection(@ConnectedSocket() client: Socket): void {
    console.log(`Client with id: ${client.id} connected`);
  }

  handleDisconnect(@ConnectedSocket() client: Socket): void {
    console.log(`Client with id: ${client.id} disconnected`);
  }
}
