import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Message } from './message.entity';
import { Participant } from './participant.entity';

@Entity('chats')
export class Chat {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(() => Participant, (participant) => participant.chat, {
    cascade: true,
  })
  participants: Participant[];

  @OneToMany(() => Message, (message) => message.chat)
  messages: Message[];
}
