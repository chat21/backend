import { Module } from '@nestjs/common';
import { ChatsGateway } from './chats.gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatsService } from './services/chats.service';
import { MessagesService } from './services/messages.service';
import { ChatsRepository } from './repositories/chats.repository';
import { MessagesRepository } from './repositories/message.repository';
import { ParticipantsRepository } from './repositories/participants.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ChatsRepository,
      MessagesRepository,
      ParticipantsRepository,
    ]),
  ],
  providers: [ChatsGateway, ChatsService, MessagesService],
})
export class ChatsModule {}
