import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatsModule } from './chats/chats.module';
import { User } from './users/user.entity';
import { Chat } from './chats/entities/chat.entity';
import { Message } from './chats/entities/message.entity';
import { Participant } from './chats/entities/participant.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: 'postgres://exjmcwma:cySSK5JRQuEe8zZcR-Sv3oDfXPm6GcVf@abul.db.elephantsql.com/exjmcwma',
      synchronize: true,
      entities: [User, Chat, Message, Participant],
      logging: false,
    }),
    UsersModule,
    AuthModule,
    ChatsModule,
  ],
})
export class AppModule {}
