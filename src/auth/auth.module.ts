import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersRepository } from 'src/users/users.repository';
import { WsJwtStrategy } from './ws-jwt.strategy';
import { WsJwtAuthGuard } from './ws-jwt-auth.guard';

@Module({
  imports: [
    TypeOrmModule.forFeature([UsersRepository]),
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: 'secret-key',
      signOptions: {
        expiresIn: '24h',
      },
    }),
  ],
  providers: [AuthService, WsJwtStrategy, WsJwtAuthGuard],
  controllers: [AuthController],
})
export class AuthModule {}
