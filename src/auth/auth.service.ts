import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { User } from 'src/users/user.entity';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { SignInDto } from './dto/sign-in.dto';
import { ResponseSignInDto } from './dto/response-sign-in.dto';
import { JwtService } from '@nestjs/jwt';
import { plainToInstance } from 'class-transformer';
import { ResponseSignUpDto } from './dto/response-sign-up.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn({ username, password }: SignInDto): Promise<ResponseSignInDto> {
    const user = await this.usersService.findOne({
      where: { username: username },
    });

    if (!user) {
      throw new UnauthorizedException('Unauthorized');
    }

    const isRealPassword = await bcrypt.compare(password, user.password);

    if (!isRealPassword) {
      throw new UnauthorizedException('Unauthorized');
    }

    const payload = { username: username, sub: user.id };
    return {
      jwtToken: this.jwtService.sign(payload),
    };
  }

  async signUp(dto: CreateUserDto): Promise<ResponseSignUpDto> {
    const candidate = await this.usersService.findOne({
      where: { username: dto.username },
    });
    if (candidate) {
      throw new BadRequestException(
        `User with username: ${dto.username} already exists`,
      );
    }

    const newUser = await this.usersService.create(dto);
    const jwtToken = this.jwtService.sign({
      username: newUser.username,
      password: newUser.password,
    });

    return {
      jwtToken,
      user: plainToInstance(User, newUser),
    };
  }
}
