import { ResponseSignInDto } from './response-sign-in.dto';
import { User } from 'src/users/user.entity';

export class ResponseSignUpDto extends ResponseSignInDto {
  user: User;
}
