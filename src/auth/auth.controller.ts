import { Body, Controller, Post } from '@nestjs/common';

import { AuthService } from './auth.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { SignInDto } from './dto/sign-in.dto';
import { ResponseSignInDto } from './dto/response-sign-in.dto';
import { ResponseSignUpDto } from './dto/response-sign-up.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

@ApiTags('auth')
@ApiBearerAuth()
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOkResponse({
    type: ResponseSignInDto,
  })
  @ApiUnauthorizedResponse()
  @ApiBody({ type: SignInDto })
  @Post('signIn')
  async login(@Body() dto: SignInDto): Promise<ResponseSignInDto> {
    return this.authService.signIn(dto);
  }

  @ApiOkResponse({
    type: ResponseSignInDto,
  })
  @ApiUnauthorizedResponse()
  @ApiBody({ type: CreateUserDto })
  @Post('signUp')
  async registration(@Body() dto: CreateUserDto): Promise<ResponseSignUpDto> {
    return this.authService.signUp(dto);
  }
}
